/*
        E-COMMERCE DATA MODEL

    USERS
        _id - <db_generated_id>        
        username - string
        password - string
        user_info - <ref_userInfo_ID>

    USER_INFO
        _id - <db_generated_id>        
        first_name - string
        last_name - string
        age - number
        address - string or sub-document
        contact - string or sub-document
        _access_id - <ref_access_ID>

    ACCESS
        _id - <db_generated_id>
        level - number
        control - string
        description - string        


    CARTS
        _id - <db_generated_id>
        user_id - <emb_userId>
        cart_items - sub-document      // [<ref_product_id>,<ref_product_id>,<ref_product_id>]
        total - number


    ORDERS
        _id - <db_generated_id>        
        cart_ID - <ref_carts_ID>        
        status - string      
        timestamp - date
        source - string
        destination - string        


    PRODUCTS
        _id - <db_generated_id>
        _sku - number
        description - string
        price - number 
        brand - string 
        manufacturer - string      
        source - string             // <emb_userID, company_id>
        category - string        
*/