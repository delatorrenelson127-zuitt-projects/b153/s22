

/*
    
    Database - All the collections compiled for a service/application's data.
        e.g - A Cabinet
    Collections - Separate categories compiled for a service/application's data
        e.g - Division/Drawer on a Cabinet
    Documents - Each specific individual record.
        e.g - Folders inside drawer
            - Specific User
            - Specific product
    Sub-documents - Specific information about a record
        e.g. file inside a folder
    Fields - Each records' specific information + their data type
        e.g File/folder content
            - User's name = string
            - User's age = number


    When starting to model your data, the very first question you ned to ask is 
        'What kind of information will I need to save to make my app/service work?'

    e.g A Blog Website

    Users
        - ID (MongoDB automatically adds an ID to all documents/records)
        - Username
        - Password
        - Email
        - User type
*/

/*
	DATA MODEL DESIGN
	Data Model Design is the step where the structure of all your service/application's data is decided, much like a blueprint is needed before building a house can begin.

	Database:
	All the collections compiled for a service/application's data
	e.g. a filing cabinet

	Collections:
	Separate categories compiled for a service/application's data
	e.g. filing cabinet drawers
		-Users collection
		-Products collection
		-Orders collection

	Documents:
	Each specific, individual records of resources
	e.g. Folders inside drawers
		-Specific user
		-Specific product
		-Specific order

	Sub-documents:
	Specific information about a record that is compiled similarly to a document, but is inside of a document itself
	e.g. file inside a folder
		-User orders
		-User contact ontact info
		-User comments

	Fields:
	Each record's specific information + their data type
	e.g. File/folder content
		-User's name = string
		-User's age = number
		-User's email = string
		-User's password = string

*/

//When starting to model your data, the very first question you need to ask is "What kind of information will I need to save to make my app/service work?"

//e.g. a blog

//-Users:
	//ID - MongoDB Object ID (MongoDB automatically adds an ID to all documents/records)
	//username - string
	//password - string
	//email - string
	//isAdmin: boolean

//-Content/Posts
	//ID - MongoDB Object ID (MongoDB automatically adds an ID to all documents/records)
	//title - string
	//body - string
	//datePosted - date
	//author - MongoDB Object ID/string
		//e.g.
		//author: ObjectId("507f1f77bcf86cd799439011")
		//OR
		//author: "ObjectId("507f1f77bcf86cd799439011")"

//EMBEDDED VS. REFERENCED DATA

/*
 When data is embedded, it is included as a direct part (called a subdocument) of any other data that has a relationship with it

 Users document:
 {
	id: <ObjectId1>,
	user: "123xyz",
	address: {
		street: "123 Street st",
		city: "New York",
		country: "United States"
	}
 }

 //When data is referenced, any other data that needs to be linked must be referenced, ideally via ID.

 	Users document:
	{
		_id: <ObjectId1>,
		user: "123xyz"
	}

 	Addresses document:
	{
		_id: <ObjectId2>,
		userId: <ObjectId1>
		street: "123 Street st",
		city: "New York",
		country: "United States"
	}

	When to use embedded vs referenced:

	Embedded vs. referenced data often only has "suggestions" instead of rules. If you believe that your data is more readable/makes more sense in one way, go ahead and so. Generally, embedded data is easier to use and understand, EXCEPT for one specific scenario:

	If you expect the data that you will embed will continuously grow in size, it is safer to move that data outside the document and into its own collection, then just use references.

	Example:
	
	User document:
	{
		_id: <ObjectId1>,
		user: "123xyz",
	}

	Product document:
	{
		_id: <ObjectId2>,
		productName: "Colgate",
		category: "toiletries",
		price: 50,
		isAvailable: true
	}

	{
		_id: <ObjectId3>,
		productName: "Cheetos",
		category: "snacks",
		price: 5,
		isAvailable: true
	}

	Order document:
	{
		_id: <ObjectId4>,
		userId: <ObjectId1>
		products: [
			{
				productId: <ObjectId2>
				qty: 2 
			},
			{
				productId: <ObjectId3>
				qty: 1 	
			}
		],
		totalPrice: 105,
		purchasedOn: 2-2-2022
	}

*/
